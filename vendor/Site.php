<?php 
	class Site {
		
		
		////////////////////////////////////////////////////////////////
		// DEV / DEBUG /////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////
		
		public static function sp( $q, $hide = false, $title = false ) {
			
			echo '<pre '.(($hide)?'style="display:none;"':'').'>';
			if ( $title ) echo $title.'<br />-------------------------------<br />';
			print_r( $q );
			echo '</pre>';
        }
        
        public static function sd( $q, $hide = false, $title = false ) {
			
			echo '<pre '.(($hide)?'style="display:none;"':'').'>';
			if ( $title ) echo $title.'<br />-------------------------------<br />';
			var_dump( $q );
			echo '</pre>';
        }
		
		public static function VPost( $POST ) {
			
			foreach ( $POST as $k => $v ) $POST[stripslashes( htmlspecialchars( strip_tags( trim( $k ))))] = stripslashes( htmlspecialchars( strip_tags( trim( $v ))));
			return $POST;
		}
		
		
		////////////////////////////////////////////////////////////////
		// Init ////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////
		
		
		public function __construct( $arConf = [] ) {
			
			$this->Conf			= $arConf;
			
			$this->MD			= new Mobile_Detect;
			$this->MySQL		= new SafeMySQL( $arConf['DB'] );
			$this->Xlsx			= new SimpleXLSX;
			
//			$this->AvailCars	= json_decode(file_get_contents( __DIR__.'/../data/json/avail_cars.json' ));
			$this->AvailCars	= $this->getCars();
			$this->Meta			= $this->getMeta();
		}
		
		public function Conf() {
			
			return $this->Conf;
		}
		
		public static function getWorld( $q = 0, $flag = 'd' ) {
			
			$res = [
				'd' => ['день', 'дня', 'дней'],
				'h' => ['час', 'часа', 'часов'],
				'm' => ['минута', 'минуты', 'минут'],
				's' => ['секунда', 'секунды', 'секунд'],
				'a' => ['автомобиль', 'автомобиля', 'автомобилей'],
			];
			
			$test = [
				[1, 21, 31, 41, 51, 61, 71, 81, 91, 101, 121, 131, 141, 151, 161, 171, 181, 191],
				[2,3,4,22,23,24,32,33,34,42,43,44,52,53,54,62,63,64,72,73,74,82,83,84,92,93,94,102,103,104,122,123,124,132,133,134,142,143,144,152,153,154,162,163,164,172,173,174,182,183,184,192,193,194]
			];
			
			if ( in_array( (int)$q, $test[0] ) ) return $res[$flag][0];
			if ( in_array( (int)$q, $test[1] ) ) return $res[$flag][1];
			return $res[$flag][2];
		}
		
		
		////////////////////////////////////////////////////////////////
		// Brands  /////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////
		
		public function getBrands() {
			
			return $this->MySQL->getAll('SELECT * FROM brands');
		}
		
		public function getAvailBrands() {
			
			$brand_ids = $this->MySQL->getCol('SELECT brand_id FROM items GROUP BY brand_id');
			return ( $brand_ids ) ? $this->MySQL->getInd('alias', 'SELECT * FROM brands WHERE id IN (?a) ORDER BY title', $brand_ids) : false;
		}
		
		public function getBrandById( int $id ) {
			
			return $this->MySQL->getRow('SELECT * FROM brands WHERE id = ?i', $id);
		}
		
		public function getBrandByExtId( string $id ) {
			
			return $this->MySQL->getRow('SELECT * FROM brands WHERE ext_id = ?s', $id);
		}
		
		public function getBrandByExtAlias( string $alias ) {
			
			return $this->MySQL->getRow('SELECT * FROM brands WHERE alias = ?s', $alias);
		}
		
		public function getBrandByTitle( string $title ) {
			
			return $this->MySQL->getRow('SELECT * FROM brands WHERE title = ?s', $title);
		}
		
		public function getBrandByAlias( string $alias) {
			
			return $this->MySQL->getRow('SELECT * FROM brands WHERE alias = ?s', $alias);
		}
		
		public function setBrand( $POST ) {
			
			$arIns = $POST;
			unset( $arIns['id'] );
			
			if ( $POST['id'] ) {
				
				$this->MySQL->query('UPDATE brands SET ?u WHERE id = ?i', $arIns, (int)$POST['id']);
				$res = ['status'=>'update'];
			
			} else {
			
				$this->MySQL->query('INSERT INTO brands SET ?u', $arIns);
				$res = ['status'=>'insert'];
			}
			
			return $res;
		}
		
		public function delBrands( $q ) {
			
			$res = $this->MySQL->getOne('SELECT COUNT(id) FROM brands WHERE ext_id NOT IN (?a)', $q);
			$this->MySQL->query('DELETE FROM brands WHERE ext_id NOT IN (?a)', $q);
			
			return $res;
		}
		
		////////////////////////////////////////////////////////////////
		// Models  /////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////
		
		public function getModels() {
			
			return $this->MySQL->getAll('SELECT * FROM models');
		}
		
		public function getAvailModels() {
			
			$model_ids = $this->MySQL->getCol('SELECT model_id FROM items GROUP BY model_id');
			return ( $model_ids ) ? $this->MySQL->getInd('alias', 'SELECT * FROM models WHERE id IN (?a) ORDER BY brand_title', $model_ids) : false;
		}
		
		public function getAvailModelsByBrand( int $id ) {
			
			$model_ids = $this->MySQL->getCol('SELECT model_id FROM items GROUP BY model_id');
			return ( $model_ids ) ? $this->MySQL->getInd('alias', 'SELECT alias, title, rutitle FROM models WHERE id IN (?a) AND brand_id = ?i ORDER BY brand_title', $model_ids, $id) : false;
		}
		
		public function getModelById( int $id ) {
			
			return $this->MySQL->getRow('SELECT * FROM models WHERE id = ?i', $id);
		}
		
		public function getModelsByBrand( int $id ) {
			
			return $this->MySQL->getAll('SELECT * FROM models WHERE brand_id = ?i', $id);
		}
		
		public function getModelByExtId( string $id ) {
			
			return $this->MySQL->getRow('SELECT * FROM models WHERE ext_id = ?s', $id);
		}
		
		public function getModelByAlias( string $q ) {
			
			return $this->MySQL->getRow('SELECT * FROM models WHERE alias = ?s', $q);
		}
		
		public function getModelByTitle( string $q ) {
			
			return $this->MySQL->getRow('SELECT * FROM models WHERE title = ?s', $q);
		}
		
		public function setModel( $POST ) {
			
			$arIns = $POST;
			unset( $arIns['id'] );
			
			if ( $POST['id'] ) {
				
				$this->MySQL->query('UPDATE models SET ?u WHERE id = ?i', $arIns, (int)$POST['id']);
				$res = ['status'=>'update'];
			
			} else {
			
				$this->MySQL->query('INSERT INTO models SET ?u', $arIns);
				$res = ['status'=>'insert'];
			}
			
			return $res;
		}
		
		public function delModels( $q ) {
			
			$res = $this->MySQL->getOne('SELECT COUNT(id) FROM models WHERE ext_id NOT IN (?a)', $q);
			$this->MySQL->query('DELETE FROM models WHERE ext_id NOT IN (?a)', $q);
			
			return $res;
		}
		
		////////////////////////////////////////////////////////////////
		// Items  //////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////
		
		public function getItems() {
			
			return $this->MySQL->getAll('SELECT * FROM items');
		}
		
		public function getCountItems() {
			
			return $this->MySQL->getOne('SELECT COUNT(id) FROM items');
		}
		
		public function getItemsByBrand( int $id ) {
			
			return $this->MySQL->getAll('SELECT * FROM items WHERE brand_id = ?i', $id);
		}
		
		public function getCountItemsByBrand( int $id ) {
			
			return $this->MySQL->getOne('SELECT COUNT(id) FROM items WHERE brand_id = ?i', $id);
		}
		
		public function getItemsByModel( int $id ) {
			
			return $this->MySQL->getAll('SELECT * FROM items WHERE model_id = ?i', $id);
		}
		
		public function getCountItemsByModel( int $id ) {
			
			return $this->MySQL->getOne('SELECT COUNT(id) FROM items WHERE model_id = ?i', $id);
		}
		
		public function getPricesByBrand( int $id ) {
		}
		
		public function getPricesByModel( int $id ) {
			
			$res['price'] = $this->MySQL->getOne('SELECT MIN(price) FROM items WHERE model_id = ?i', $id);
			$res['discount'] = $this->MySQL->getOne('SELECT MAX(discount) FROM items WHERE model_id = ?i', $id);
			$res['final_price'] = $this->MySQL->getOne('SELECT MIN(final_price) FROM items WHERE model_id = ?i', $id);
			
			return $res;
		}
		
		public function getItemById( int $id ) {
			
			return $this->MySQL->getRow('SELECT * FROM items WHERE id = ?i', $id);
		}
		
		public function getItemByVIN( string $q ) {
			
			return $this->MySQL->getRow('SELECT * FROM items WHERE vin = ?s', $q);
		}
		
		public function setItem( $POST ) {
		
			$arIns = $POST;
			unset( $arIns['id'] );
			
			if ( $POST['id'] ) {
				
				$this->MySQL->query('UPDATE items SET ?u WHERE id = ?i', $arIns, (int)$POST['id']);
				$res = ['status'=>'update'];
			
			} else {
			
				$this->MySQL->query('INSERT INTO items SET ?u', $arIns);
				$res = ['status'=>'insert'];
			}
			
			return $res;
		}
		
		public function delItems( $q ) {
			
			$res = $this->MySQL->getOne('SELECT COUNT(id) FROM items WHERE vin NOT IN (?a)', $q);
			$this->MySQL->query('DELETE FROM items WHERE vin NOT IN (?a)', $q);
			
			return $res;
		}
		
		public function importItems( $f ) {
			
			$xlsx = $this->Xlsx::parse( $f );
			$res['success'] = ['new'=>0, 'upd'=>0];
			
			foreach ( $xlsx->rows() as $k => $row ) {
				
				if ( $k == 0 ) continue;
				
				if ( $b = $this->getBrandByTitle( $row[0] ) ) {
					
					if ( $m = $this->getModelByTitle( $row[1] ) ) {
						
						$arIns = [
							'brand_id' => $b['id'],
							'model_id' => $m['id'],
							'vin' => (string)$row[3],
							'price' => (int)$row[4],
							'discount' => (int)$row[8],
							'final_price' => (int)$row[9]
						];
						
						if ( $i = $this->getItemByVIN( (string)$row[3] ) ) $arIns['id'] = $i['id'];
						
						( $this->setItem( $arIns ) == 'insert' ) ? $res['success']['new']++ : $res['success']['upd']++;
						$res['vins'][] = (string)$row[3];
						
					} else {
						
						$res['error']['models'][] = $row;
					}
					
				} else {
					
					$res['error']['brands'][] = $row;
				} 
			}
			
			$res['success']['del'] = ( $res['vins'] ) ? $this->delItems( $res['vins'] ) : 0;
			
			return $res;
		}
		
		
		////////////////////////////////////////////////////////////////
		// Auth  ///////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////
		
		public function getUserByName( string $name ) {
			
			return $this->MySQL->getRow('SELECT * FROM users WHERE name = ?s', $name);
		}
		
		public function checkAUth() {
			
			return ($_SESSION['SSID']) ? true : false;
		}
		
		public function unAUth() {
			
			session_destroy();
			header('Location: /');
		}
		
		public function AUth($POST) {
				
			if ( $POST['name'] == preg_replace('/[^a-zA-Z0-9а-яА-Я@._\-\s]/', '', $POST['name']) ) {
				
				$user = $this->getUserByName( mb_strtolower($POST['name']) );
				
				if ( $user ) {
					
					if ( password_verify($POST['passwd'], $user['passwd']) ) {
						
						$_SESSION['SSID'] = $user['ssid'];
						$res = $res = ['status'=>'success', 'desc'=>''];
						
					} else {
						
						$res = ['status'=>'error', 'desc'=>'Неверный пароль 304'];
					}
					
				} else {
					
					$res = ['status'=>'error', 'desc'=>'Неверный логин 309'];
				}
				
			} else {
				
				$res = ['status'=>'error', 'desc'=>'Неверный логин 314'];
			}
			
			return (object)$res;
		}
		
		
		////////////////////////////////////////////////////////////////
		// Phone  //////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////
		
		public static function phoneIn( $phone ) {
			
			// +7 (111) 111-11-11 -> 71111111111
			
			$phone = preg_replace('/[^0-9]/', '', $phone);
			$phone = mb_substr($phone, 0, 11);
			
			if ( mb_strlen($phone) == 10 ) $phone = '7'.$phone;
			if ( mb_strlen($phone) == 7 ) $phone = '7861'.$phone;
			if ( $phone[0] == '8' ) $phone = '7'.mb_substr($phone, 1);
			
			return $phone;
		}
		
		public static function phoneOut( $str ) {
			
			// 71111111111 -> +7 (111) 111-11-11
			
			$str = self::phoneIn( $str );
			
			for ($k = 0; $k < mb_strlen((string)$str); $k++) $phone[] = mb_substr($str, $k, 1);
			return '+'.$phone[0].' ('.$phone[1].$phone[2].$phone[3].') '.$phone[4].$phone[5].$phone[6].'-'.$phone[7].$phone[8].'-'.$phone[9].$phone[10];
		}
		
		
		////////////////////////////////////////////////////////////////
		// Routing  ////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////
		
		public function Route() {
			
			$RequestURL = parse_url($_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'])['path'];
			$url = explode('/', $RequestURL);
			$includeFile = $_SERVER['DOCUMENT_ROOT'].'/data/views/view_'.(($url[1])?:'index').'.php';

			$res['route_file'] = ( file_exists( $includeFile ) ) ? $includeFile : $_SERVER['DOCUMENT_ROOT'].'/data/views/view_404.php';
			$res['section'] = $url[1];
			$res['part'] = $url[2];
			$res['data'] = $url[3];
			
			return (object)$res;
		}
		
		public function getMeta() {
			
			$RequestURL = parse_url($_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'])['path'];
			$url = explode('/', $RequestURL);
			
			$res = [
				'title' => $this->Conf()['title'],
				'h1' => $this->Conf()['h1'],
				'description' => $this->Conf()['description'],
				'keywords' => $this->Conf()['keywords'],
			];
			
			if ( $url[1] ) {
				
				if ( $this->Conf()['Meta'][$url[1]]['title'] ) $res['title'] = $this->Conf()['Meta'][$url[1]]['title'];
				if ( $this->Conf()['Meta'][$url[1]]['description'] ) $res['description'] = $this->Conf()['Meta'][$url[1]]['description'];
				if ( $this->Conf()['Meta'][$url[1]]['keywords'] ) $res['keywords'] = $this->Conf()['Meta'][$url[1]]['keywords'];
			}
			
			if ( $url[2] ) {
				
				if ( $this->Conf()['Meta'][$url[1]]['parts'][$url[3]]['title'] ) $res['title'] = $this->Conf()['Meta'][$url[1]]['parts'][$url[3]]['title'];
				if ( $this->Conf()['Meta'][$url[1]]['parts'][$url[3]]['description'] ) $res['description'] = $this->Conf()['Meta'][$url[1]]['parts'][$url[3]]['description'];
				if ( $this->Conf()['Meta'][$url[1]]['parts'][$url[3]]['keywords'] ) $res['keywords'] = $this->Conf()['Meta'][$url[1]]['parts'][$url[3]]['keywords'];
				if ( $this->Conf()['Meta'][$url[1]]['canonical'] ) $res['canonical'] = $this->Conf()['Meta'][$url[1]]['canonical'];
			}
			
			return (object)$res;
		}
		
		public function Meta() {
			
			return $this->Meta;
		}
		
		public static function BreadCrumbs( $arr ) {
			
			?>
			<ul class="p-0">
				<?php foreach ( $arr as $a ) { ?>
				<li class="d-inline-block mr-4"><a href="<?=$a['link']?>"><i class="fa fa-long-arrow-left mr-2" aria-hidden="true"></i> <?=$a['title']?></a></li>
				<?php } // foreach ?>
			</ul>
			<?php
		}
		
		
		////////////////////////////////////////////////////////////////
		// Banners  ////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////
		
		public function getBanners() {
			
			$dir = $this->Conf()['DOCUMENT_ROOT'].'/data/images/banner/'.$this->Conf()['Banner']['dir'];
			
			foreach ( array_slice( scandir($dir), 2 ) as $b ) {
				
				if ( $b != 'mob' ) {
					
					if ( $this->Route()->section ) {
						
						if ( strripos($b, '_'.$this->Route()->section) )
							
							$res[] = [
								'file' => '/data/images/banner/'.$this->Conf()['Banner']['dir'].'/'.(($this->MD->isMobile())?'mob/':'').$b,
								'link' => explode('-', $b)[0],
							];
						
					} else {
					
						$res[] = [
							'file' => '/data/images/banner/'.$this->Conf()['Banner']['dir'].'/'.(($this->MD->isMobile())?'mob/':'').$b,
							'link' => explode('-', $b)[0],
						];
					}
				}
			}
			
			return $res;
		}
		
		public function getPageBanner() {
			
			$res = false;
			
			if ( $this->Route()->section && file_exists($this->Conf()['DOCUMENT_ROOT'].'/data/images/'.$this->Route()->section) ) 
				$res = '/data/images/'.$this->Route()->section.'/banner'.(($this->MD->isMobile())?'_m':'').'.jpg?'.md5_file( $this->Conf()['DOCUMENT_ROOT'].'/data/images/'.$this->Route()->section.'/banner'.(($this->MD->isMobile())?'_m':'').'.jpg' );
			
			return $res;
		}
		
		
		
		////////////////////////////////////////////////////////////////
		// Avail Cars //////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////
		
		
		
		public function getAvailCars() {
			
			return $this->AvailCars;
		}
		
		public function getCars() {
			
			$res['items_count'] = 0;
			
			$res['brands'] = $this->getAvailBrands();
			foreach ( $res['brands'] as $k => $i ) {
				
				$res['brands'][$k]['count'] = $this->getCountItemsByBrand( $i['id'] );
				$res['brands'][$k]['models'] = $this->getAvailModelsByBrand( $i['id'] );
			}
			
			$res['models'] = $this->getAvailModels();
			foreach ( $res['models'] as $k => $i ) {
				
				$res['models'][$k]['count'] = $this->getCountItemsByModel( $i['id'] );
				$res['models'][$k] = array_merge( $res['models'][$k], $this->getPricesByModel($i['id']) );
				$res['prices'][] = $res['models'][$k]['final_price'];
				$res['items_count'] += $res['models'][$k]['count'];
			}
			
			sort( $res['prices'] );
			
			array_unshift( $res['prices'], intval($res['prices'][0]/10000)*10000 );
			array_push( $res['prices'], intval(($res['prices'][count($res['prices'])-1]+5000)/10000)*10000 );
			
			uasort( $res['models'], function( $a, $b ) {
		
				if ( $a['final_price'] == $b['final_price'] ) return 0;
				return ( $a['final_price'] < $b['final_price'] ) ? -1 : 1;
			});
			
			$res['perPage'] = $this->Conf()['page'];
			$res['Count'] = count( $res['models'] );
			
			return $res;
		}
		
		
		
		////////////////////////////////////////////////////////////////
		// Actions /////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////
		
		public function getActionsList() {
			
			$dir = $this->Conf()['DOCUMENT_ROOT'].'/data/views/actions/detail/sales';
			foreach ( array_slice( scandir($dir), 2 ) as $a ) {
				
				$res['sales'][] = explode('.', $a)[0];
			}
			
			$dir = $this->Conf()['DOCUMENT_ROOT'].'/data/views/actions/detail/service';
			foreach ( array_slice( scandir($dir), 2 ) as $a ) {
				
				$res['service'][] = explode('.', $a)[0];
			}
			
			return $res;
		}
		
		public function getActionImage( $url ) {
			
			return '/data/images/actions/detail/'.$url.(($this->MD->isMobile())?'_m':'').'.jpg';
		}
		
		public function getDetailFile( $url, $id ) {
			
			return $_SERVER['DOCUMENT_ROOT'].'/data/views/actions/detail/'.$url.'/'.$id.'.php';
		}
		
		public function getPreviewFile( $url, $id ) {
			
			return $_SERVER['DOCUMENT_ROOT'].'/data/views/actions/preview/'.$url.'/'.$id.'.php';
		}
		
		public function getDetailLink( $file ) {
			
			$ar = explode('/', $file);
			return explode('.', $ar[count($ar)-1])[0];
		}
		
		
		
		////////////////////////////////////////////////////////////////
		// Forms  //////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////
		
		public function getForms( $modal = false ) {
			
			$dir = $this->Conf()['DOCUMENT_ROOT'].'/data/forms'.(($modal)?'/modal':'');
			foreach ( array_slice( scandir($dir), 2 ) as $a ) {
				
				$res[] = explode('.', $a)[0];
			}
			
			unset( $res['modal'] );
			return $res;
		}
		
		public function getFormFile( $url, $modal = false ) {
			
			return $_SERVER['DOCUMENT_ROOT'].'/data/forms/'.(($modal)?'modal/':'').$url.'.php';
		}
		
		public function setResult( $POST, $ip ) {
			
			if ( $POST['site_id'] ) $arIns['site_id'] = (int)$POST['site_id'];
			if ( $POST['Name'] ) $arIns['name'] = $POST['Name'];
			if ( $POST['Phone'] ) $arIns['phone'] = $this->phoneIn($POST['Phone']);
			if ( $POST['Email'] ) $arIns['email'] = $POST['Email'];
			if ( $POST['Date'] ) $arIns['date'] = strtotime($POST['Dame']);
			if ( $POST['Car'] ) $arIns['car'] = $POST['Car'];
			if ( $POST['Brand'] ) $arIns['brand_id'] = $this->getBrandByAlias($POST['Brand'])['id'];
			if ( $POST['Model'] ) $arIns['model_id'] = $this->getModelByAlias($POST['Model'])['id'];
			if ( $POST['Year'] ) $arIns['year'] = (int)$POST['Year'];
			if ( $POST['Transmission'] ) $arIns['transmission'] = $POST['Transmission'];
			if ( $POST['Milleage'] ) $arIns['milleage'] = (int)$POST['Milleage'];
			if ( $POST['Works'] ) $arIns['works'] = $POST['Works'];
			if ( $POST['Comment'] ) $arIns['comment'] = $POST['Comment'];
			if ( $POST['YandexVisitorID'] ) $arIns['yandex_visitorId'] = $POST['YandexVisitorID'];
			if ( $POST['GoogleVisitorID'] ) $arIns['google_visitorId'] = explode('.', $POST['GoogleVisitorID'])[2].'.'.explode('.', $POST['GoogleVisitorID'])[3];
			if ( $POST['MatomoVisitorID'] ) $arIns['matomo_visitorId'] = explode('.', $POST['MatomoVisitorID'])[0];
			if ( $POST['Name'] ) $arIns['visitor_ip'] = $POST['name'];
			$arIns['timestamp'] = time();
			
			return $this->MySQL->query('INSERT INTO results SET ?u', $arIns);
		}
		
	}
	
	