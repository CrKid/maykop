<?php
	
	return [
		
		'DOCUMENT_ROOT' => $_SERVER['DOCUMENT_ROOT'],
		
		
		
		'phone' => '8 (861) 212-77-00',
		'address' => 'г. Майкоп, ул. Хакурате, 648Б',
		'email' => 'maykop@yug-avto.ru',
		'map' => '<script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A2d1a821f5a2ebc6c067231b5870fe81029c00135ad29b1f1f661224f0eb31b96&amp;width=100%25&amp;height=500&amp;lang=ru_RU&amp;scroll=true"></script>',
		
		'calltouch' => 'class_for_calltouch',
		
		'title' => 'Купить новый автомобиль в Майкопе | Продажа машин в Адыгее, цены | Юг-Авто Центр Майкоп',
		'h1' => 'Юг-Авто Центр Майкоп. Продажа и сервисное обслуживание автомобилей.',
		'description' => 'Продажа новых автомобилей в салоне Юг-Авто Майкоп. Купить иномарку в кредит, цены на легковые машины.',
		'keywords' => '',
		
		'Meta' => [
			'actions' => [
				'title' => 'Акции и спецпредложения. Юг-Авто Центр Майкоп.',
				'description' => '',
				'keywords' => '',
				'canonical' =>  false, //'https://maykop.yug-avto.ru/action/',
				'parts' => [
					'opened' => [
						'title' => 'Мы пришли и будем удивлять! Юг-Авто Центр Майкоп.',
						'description' => '',
						'keywords' => ''
					],
					'10points_complex_diagnostic' => [
						'title' => 'Бесплатная комплексная диагностика по 10 пунктам. Юг-Авто Центр Майкоп.',
						'description' => '',
						'keywords' => ''
					],
					'oil' => [
						'title' => 'Замена масла для вашего автомобиля от 1 990 ₽. Юг-Авто Центр Майкоп.',
						'description' => '',
						'keywords' => ''
					]
				]
			],
			'service' => [
				'title' => 'Ремонт и диагностика автомобиля в Майкопе | Техническое обслуживание от Юг-Авто',
				'description' => 'Запись на техническое обслуживание, диагностику и ремонт автомобиля в Юг-Авто',
				'keywords' => '',
			],
			'hot' => [
				'title' => 'Горячие предложения месяца. Юг-Авто Центр Майкоп.',
				'description' => '',
				'keywords' => '',
			],
			'tradein' => [
				'title' => 'Трейд ин: оценка и продажа авто с пробегом в Юг-Авто',
				'description' => 'Выгодные условия Trade In от салона Юг-Авто. Оценить и сдать машину в трейд ин.',
				'keywords' => '',
			],
			'contacts' => [
				'title' => 'Контакты. Юг-Авто Центр Майкоп.',
				'description' => '',
				'keywords' => '',
			],
		],
		
		'Social' => [
			'facebook' => 'https://www.facebook.com/yugavto',
			'vk' => 'https://vk.com/yugavto',
			'instagram' => 'https://instagram.com/yugavto/',
			'youtube' => 'https://www.youtube.com/user/webyugavto'
		],
		
		'page' => 12,
		
		'Coords' => [
			'lat' => '44.623539',
			'lon' => '40.057540'
		],
		
		'Recipients' => [
			'Global' => [
				'callcenter@adv.yug-avto.ru',
				'yuriy.bubnov@yug-avto.ru',
				'nataliya.ivanova@yug-avto.ru',
				'andrey.taravkov@lada.yug-avto.ru',
				'inessa.aleshina@yug-avto.ru',
				'dmitriy.smirnov@yug-avto.ru'
			],
			'testdrive' => [
				//'anton.boreckiy@yug-avto.ru'
			],
		],
		
		'Banner' => [
			'dir' => 'current',
			'Links' => [
				'01' => '/actions/sales/no_overpayment/',
				'02' => '/actions/sales/first_and_family/',
				'03' => '/actions/sales/credit_0_percent/',
				'04' => '/actions/service/10points_complex_diagnostic/',
				'05' => '/actions/service/oil/',
				'06' => '/actions/service/fitting/',
				'07' => '/actions/service/conditioning/',
				'08' => '/actions/service/clear-and-deodorization/'
			],
		],
		
		'Forms' => [
			'form_offer' => [
				'title' => 'Заполни форму - узнай цену'
			],
		],
	];


?>