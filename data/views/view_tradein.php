	<div class="container breadcrumbs mb-5">
      <div class="row">
        <div class="col-md-12"><?php $app::BreadCrumbs( [['title'=>'Главная', 'link'=>'/']] ) ?></div>
      </div>
    </div>
    
    <div class="container-fluid">
      <div class="row">
        <div class="col text-center mb-5">
          <img class="img-fluid" alt="<?=$app->Meta()->title?>" src="<?=$app->getPageBanner()?>" />
        </div>
      </div>
    </div>
    
    <div class="container mb-5">
      <div class="row">
        <div class="col-md-12">
          <h1>Трейд-ин</h1>
          <p>Юг-Авто Центр Майкоп предлагает своим клиентам воспользоваться программой trade-in.<br />Данная программа позволяет удобно и быстро обменять свой автомобиль на новый и при этом получить весомую выгоду.</p>
        </div>
      </div>
    </div>
    
    <div class="py-5 bg-yalightgray">
      <div class="container">
        <?php include $app->getformFile( 'form_tradein' ); ?>
      </div>
    </div>