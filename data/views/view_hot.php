	<div class="container breadcrumbs mb-5">
      <div class="row">
        <div class="col-md-12"><?php $app::BreadCrumbs( [['title'=>'Главная', 'link'=>'/']] ) ?></div>
      </div>
    </div>
    
    <?php if ( $page_banner = $app->getPageBanner() ) { ?>
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12 text-center mb-5">
          <img class="img-fluid" src="<?=$page_banner?>" />
        </div>
      </div>
    </div>
    <? } ?>
    
    <div class="container my-5">
      <div class="row">
        <div class="col-md-12 mb-5">
          <h1 class="mb-3">Хотите получить больше, чем ожидаете?<br />Тогда ждем вас дилерском центре Юг-Авто Центр Майкоп!</h1>
          <p>Вы можете заранее забронировать понравившийся вам автомобиль из списка ниже либо узнать о предложениях, которых нет на сайте, позвонив по телефону, ведь мы можем вам предложить гораздо больше!</p>
        </div>
        <div class="col-md-12">
          <div id="YApps_Hot"></div>
        </div>
      </div>
    </div>
    
    <div class="py-5 bg-yalightgray" role="scrole" data-scrole="help">
      <div class="container">
        <?php include $app->getFormFile( 'form_callback' ); ?>
      </div>
    </div>