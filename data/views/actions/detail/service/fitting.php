﻿<h1>Только до 31 января!</h1>
<p>Воспользуйтесь выгодным предложением от Юг-Авто Центр Майкоп!</p>
<h3>Шиномонтаж + мойка автомобиля и диагностика ходовой в подарок!</h3>
<ul>
  <li>R15 - 900 ₽</li>
  <li>R16 – 1000 ₽</li>
</ul>
<p>Получить более подробную информацию, проконсультироваться со специалистом и записаться на обслуживание вы можете по телефону:</p>
<p><a href="tel:+<?=$app::phoneIn( $app->Conf()['phone'] )?>" class="phone"><i class="fa fa-phone" aria-hidden="true"></i> <?=$app::phoneOut( $app->Conf()['phone'] )?></a></p>
<?php $actionForm = 'form_service';?>