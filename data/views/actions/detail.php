<div class="container breadcrumbs mb-5">
  <div class="row">
	<div class="col-md-12"><?php $app::BreadCrumbs( [['title'=>'Главная', 'link'=>'/'], ['title'=>'Акции и спецпредложения', 'link'=>'/actions/']] ) ?></div>
  </div>
</div>

<div class="container-fluid">
  <div class="row">
	<div class="col text-center mb-5">
	  <img class="img-fluid" alt="<?=$app->Meta()->title?>" src="<?=$app->getActionImage( $app->Route()->data )?>?<?=md5_file( $_SERVER['DOCUMENT_ROOT'].$app->getActionImage( $app->Route()->data ) )?>" />
	</div>
  </div>
</div>

<div class="container mb-5">
  <div class="row">
	<div class="col-md-12">
	  <?php include $app->getDetailFile( $app->Route()->part, $app->Route()->data ); ?>
	</div>
  </div>
</div>
<?php if ( $actionMap ) { ?>
<div class="container-fluid">
  <div class="row">
    <div class="col-md-12">
      <?=$app->Conf()['map']?>
    </div>
  </div>
</div>
<?php } // if Map ?>
<?php if ( $actionForm ) { ?>
<div class="py-5 bg-yalightgray">
  <div class="container">
    <?php include $app->getFormFile( $actionForm ); ?>
  </div>
</div>
<?php } // if Form ?>

<?php if ( $disclamer ) { ?>
<div class="py-4">
  <div class="container">
    <div class="col-md-12"><p><small><?=$disclamer?></small></p></div>
  </div>
</div>
<?php } // if Form ?>