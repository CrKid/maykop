<div class="container breadcrumbs mb-5">
  <div class="row">
	<div class="col-md-12"><?php $app::BreadCrumbs( [['title'=>'Главная', 'link'=>'/']] ) ?></div>
  </div>
</div>

<div class="container mb-5">
  <div class="row">
	<div class="col-md-12 mb-5"><h1>Акции и спецпредложения</h1></div>
    <div class="col-md-12 mb-3" role="scrole" data-scrole="sales"><h3>Спецпредложения</h3></div>
  	<?php foreach ( $app->getActionsList()['sales'] as $a ) { ?>
	  <div class="col-md-6 mb-3">
	    
		<div class="card border-secondary">
		  <div class="card-body">
			<a href="/actions/sales/<?=$a?>/"><img class="img-fluid mb-2" src="<?=$app->getActionImage( $a )?>?<?=md5_file( $_SERVER['DOCUMENT_ROOT'].$app->getActionImage( $a ) )?>" /></a>
			<?php include $app->getPreviewFile( 'sales', $a ); ?>
            <hr />
            <a href="/actions/sales/<?=$a?>/" class="but but-white py-2 d-block text-left">Подробнее <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
		  </div>
		</div>
	  </div>
	<?php } // foreach ?>
    <div class="col-md-12 my-2"><hr /></div>
    <div class="col-md-12 mb-3" role="scrole" data-scrole="service"><h3>Сервисные акции</h3></div>
    <?php foreach ( $app->getActionsList()['service'] as $a ) { ?>
	  <div class="col-md-6 mb-3">
	    
		<div class="card border-secondary">
		  <div class="card-body">
			<a href="/actions/service/<?=$a?>/"><img class="img-fluid mb-2" src="<?=$app->getActionImage( $a )?>?<?=md5_file( $_SERVER['DOCUMENT_ROOT'].$app->getActionImage( $a ) )?>" /></a>
			<?php include $app->getPreviewFile( 'service', $a ); ?>
            <hr />
            <a href="/actions/service/<?=$a?>/" class="but but-white py-2 d-block text-left">Подробнее <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
		  </div>
		</div>
	  </div>
	<?php } // foreach ?>
  </div>
</div>