<?php
	header('HTTP/1.1 404 Not Found');
    header('Status: 404 Not Found');
?>
	<div class="container">
        <div class="row my-5 py-5 text-center">
            <div class="col-md-12">
                <div class="error-template">
                    <h1>Ой, 404!</h1>
                    <div class="error-details">
                        Кажется, мы ничего не нашли!
                    </div>
                </div>
            </div>
        </div>
    </div>