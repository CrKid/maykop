	<div class="container breadcrumbs mb-5">
      <div class="row">
        <div class="col-md-12"><?php $app::BreadCrumbs( [['title'=>'Главная', 'link'=>'/']] ) ?></div>
      </div>
    </div>
    
    <?php if ( $page_banner = $app->getPageBanner() ) { ?>
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12 text-center mb-5">
          <img class="img-fluid" src="<?=$page_banner?>" />
        </div>
      </div>
    </div>
    <? } ?>
    
    <div class="container mb-5">
      <div class="row">
        <div class="col-md-12">
          <h1>Контакты</h1>
          <p>Теперь не нужно отправляться в далекое путешествие для реализации своих планов. Юг-Авто Центр Майкоп рад предложить вам широкий выбор новых автомобилей мировых брендов, а также их качественное сервисное обслуживание.</p>
          <h3>В честь открытия нашего дилерского центра всем покупателям предоставляются индивидуальные условия покупки.</h3>
          <p>Найти нас легко:</p>
          <?php include __DIR__.'/../../include/_contacts.php'; ?>
        </div>
        <div class="col-md-12">
          <?=$app->Conf()['map']?>
        </div>
      </div>
    </div>
    
    <div class="py-5 bg-yalightgray">
      <div class="container">
        <?php include $app->getFormFile( 'form_feedback' ); ?>
      </div>
    </div>