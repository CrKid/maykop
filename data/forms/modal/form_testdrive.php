		<form name="testdrive" data-event="test-drive">
          <input type="hidden" name="Form" value="Запись на тест-драйв" />
          <input type="hidden" name="Id" value="testdrive" />
          <input type="hidden" name="Car" value="" />
          <div class="row">
            <div class="col-md-12">
              <h3>Запись на тест-драйв</h3>
              <legend></legend>
              <div class="form-group">
                <input type="phone" class="form-control" name="Name" placeholder="Ваше имя *" required />
              </div>
              <div class="form-group">
                <input type="phone" class="form-control" name="Phone" placeholder="Телефон *" required />
              </div>
              <div class="form-group">
                <a href="#" role="SendForm" class="but but-blue d-block text-center">Отправить заявку</a>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <?php include $_SERVER['DOCUMENT_ROOT'].'/include/_formpersonal.php'; ?>
              <?php include $_SERVER['DOCUMENT_ROOT'].'/include/_formresult.php'; ?>
            </div>
          </div>
        </form>