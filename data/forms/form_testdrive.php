		<form data-event="test-drive">
          <input type="hidden" name="Form" value="Запись на тест-драйв" />
          <input type="hidden" name="Id" value="testdrive" />
          <div class="row">
            <div class="col-md-12"><h3>Запись на тест-драйв</h3></div>
            <div class="col-md-3 form-group">
              <select class="form-control" name="Brand" required>
                <option selected disabled>Марка *</option>
                <?php foreach ( $app->getAvailCars()->result as $brand) { ?>
                <option value="<?=$brand->Alias?>"><?=$brand->Rutitle?></option>
                <?php } // foreach ?>
              </select>
            </div>
            <div class="col-md-3 form-group" required>
              <select class="form-control" name="Model" disabled="" required>
                <option selected disabled>Модель *</option>
              </select>
            </div>
            <div class="col-md-3 form-group">
              <input type="phone" class="form-control" name="Phone" placeholder="Телефон *" required />
            </div>
            <div class="col-md-3 form-group">
              <a href="#" role="SendForm" class="but but-blue d-block text-center">Отправить заявку</a>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <?php include $_SERVER['DOCUMENT_ROOT'].'/include/_formpersonal.php'; ?>
              <?php include $_SERVER['DOCUMENT_ROOT'].'/include/_formresult.php'; ?>
            </div>
          </div>
        </form>