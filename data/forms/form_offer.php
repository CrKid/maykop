		<form data-event="callback">
          <input type="hidden" name="Form" value="Заполни форму - узнай цену" />
          <input type="hidden" name="Id" value="offer" />
          <div class="row">
            <div class="col-md-12"><h3>Заполни форму &mdash; узнай цену</h3></div>
            <div class="col-md-3 form-group">
              <select class="form-control" name="Brand" required>
                <option selected disabled>Марка *</option>
                <?php foreach ( $app->getAvailCars()['brands'] as $brand) { ?>
                <option value="<?=$brand['alias']?>"><?=$brand['rutitle']?></option>
                <?php } // foreach ?>
              </select>
            </div>
            <div class="col-md-3 form-group" required>
              <select class="form-control" name="Model" disabled="" required>
                <option selected disabled>Модель *</option>
              </select>
            </div>
            <div class="col-md-3 form-group">
              <input type="phone" class="form-control" name="Phone" placeholder="Телефон *" required />
            </div>
            <div class="col-md-3 form-group">
              <a href="#" role="SendForm" class="but but-blue d-block text-center">Узнать цену</a>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <?php include $_SERVER['DOCUMENT_ROOT'].'/include/_formpersonal.php'; ?>
              <?php include $_SERVER['DOCUMENT_ROOT'].'/include/_formresult.php'; ?>
            </div>
          </div>
        </form>