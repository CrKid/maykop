		<form data-event="sell-car">
          <input type="hidden" name="Form" value="Заявка на трейд-ин" />
          <input type="hidden" name="Id" value="tradein" />
          <div class="row">
            <div class="col-md-12"><h3>Заявка на трейд-ин</h3></div>
            <div class="col-md-4">
              <div class="form-group">
                <input type="text" class="form-control" name="Car" placeholder="Ваш автомобиль *" required />
              </div>
              <div class="form-group">
                <input type="text" class="form-control" name="Year" placeholder="Год выпуска *" required />
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <input type="text" class="form-control" name="Engine" placeholder="Двигатель *" required />
              </div>
              <div class="form-group">
                <input type="text" class="form-control" name="Milleage" placeholder="Пробег *" required />
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <input type="text" class="form-control" name="Name" placeholder="Ваше имя *" required />
              </div>
              <div class="form-group">
                <input type="phone" class="form-control" name="Phone" placeholder="Телефон *" required />
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <textarea class="form-control" name="Comment" rows="5" placeholder="Комментарий"></textarea>
              </div>
            </div>
            <div class="col-md-4 form-group">
              <a href="#" role="SendForm" class="but but-blue d-block text-center">Отправить заявку</a>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <?php include $_SERVER['DOCUMENT_ROOT'].'/include/_formpersonal.php'; ?>
              <?php include $_SERVER['DOCUMENT_ROOT'].'/include/_formresult.php'; ?>
            </div>
          </div>
        </form>