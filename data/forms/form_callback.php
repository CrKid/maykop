		<form name="callback" data-event="callback">
          <input type="hidden" name="Form" value="Заказать обратный звонок" />
          <input type="hidden" name="Id" value="callback" />
          <div class="row">
            <div class="col-md-12"><h3>Заказать обратный звонок</h3></div>
            <div class="col-md-4 form-group">
              <input type="text" class="form-control" name="Name" placeholder="Ваше имя *" required />
            </div>
            <div class="col-md-4 form-group">
              <input type="phone" class="form-control" name="Phone" placeholder="Телефон *" required />
            </div>
            <div class="col-md-4 form-group">
              <a href="#" role="SendForm" class="but but-blue d-block text-center">Заказать звонок</a>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <?php include $_SERVER['DOCUMENT_ROOT'].'/include/_formpersonal.php'; ?>
              <?php include $_SERVER['DOCUMENT_ROOT'].'/include/_formresult.php'; ?>
            </div>
          </div>
        </form>