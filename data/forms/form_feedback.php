		<form data-event="feedback">
          <input type="hidden" name="Form" value="Обращение в компанию" />
          <input type="hidden" name="Id" value="feedback" />
          <div class="row">
            <div class="col-md-12"><h3>Обращение в компанию</h3></div>
            <div class="col-md-4">
              <div class="form-group">
                <input type="text" class="form-control" name="Name" placeholder="Ваше имя *" required />
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <input type="phone" class="form-control" name="Phone" placeholder="Телефон *" required />
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <input type="email" class="form-control" name="Email" placeholder="Email" />
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <textarea class="form-control" name="Comment" rows="5" placeholder="Ваше обращение"></textarea>
              </div>
            </div>
            <?php /*
            <div class="col-md-12">
              <div class="form-group">
                <textarea class="form-control" name="Comment" rows="5" placeholder="Комментарий"></textarea>
              </div>
            </div>
			*/ ?>
            <div class="col-md-4 form-group">
              <a href="#" role="SendForm" class="but but-blue d-block text-center">Отправить обращение</a>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <?php include $_SERVER['DOCUMENT_ROOT'].'/include/_formpersonal.php'; ?>
              <?php include $_SERVER['DOCUMENT_ROOT'].'/include/_formresult.php'; ?>
            </div>
          </div>
        </form>