<?php
#!/usr/bin/php

	/*
	ini_set('error_reporting', E_ALL);
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	*/
	
	require __DIR__.'/../vendor/autoload.php';
	
	$arConf = require __DIR__.'/../vendor/Config.php';
	require_once __DIR__.'/../vendor/Site.php';
	
	$app = new Site( $arConf );
	
	
	$j = file_get_contents('https://yug-avto.ru/car-filter/new-cars?limit=5000&seoParamUri=%2Fnew-cars');
	$o = json_decode( $j );
	$brands = []; $models = []; $items = [];
	
	foreach ( $o->filterResult->items as $i ) $items[$i->Alias] = 'https://'.$i->Photo;
	
	/**** BRANDS ****/
	foreach ( $o->filterData->Brands as $i ) {
		
		$brands[$i->Title] = [
			'ext_id' => $i->Id,
			'alias' => $i->Alias,
			'title' => $i->Title,
			'rutitle' => $i->Rutitle,
			'logo' => 'https://yug-avto.ru'.str_replace('tradeinscorp', 'holdingyugauto', $i->Logo)
		];
		
		$db[] = $i->Id;
	}
	
	foreach ( $brands as $i ) {
		
		if ( $b = $app->getBrandByExtId($i['ext_id']) ) $i['id'] = (int)$b['id'];
		$app->setBrand( $i );
	}
	
	$app->delBrands( $db );
	
	/**** MODELS ****/
	foreach ( $o->filterData->Models as $i ) {
		
		$b = $app->getBrandByTitle($i->BrandTitle);
		
		$models[] = [
			'brand_id' => (int)$b['id'],
			'ext_id' => $i->Id,
			'alias' => $i->Alias,
			'title' => $i->Title,
			'rutitle' => $i->Rutitle,
			'brand_title' => $b['title'],
			'brand_alias' => $b['alias'],
			'photo' => $items[$i->Alias]
		];
		
		$dm[] = $i->Id;
	}
	
	foreach ( $models as $i ) {
		
		if ( $b = $app->getModelByExtId($i['ext_id']) ) $i['id'] = (int)$b['id'];
		$app->setModel( $i );
	}
	
	$app->delModels( $dm );
	$app::sp( $o->filterData->Models );
?>