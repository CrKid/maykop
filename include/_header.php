<!doctype html>
<html lang="ru">
  <head>
  
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="<?=$app->Meta()->description?>">
    <meta name="keywords" content="<?=$app->Meta()->keywords?>">
    <?php if ( $app->Meta()->canonical ) { ?>
    <link rel="canonical" href="<?=$app->Meta()->canonical?>"/>
    <?php } // if ?>
    <meta name="author" content="Boretscy A">

    <title><?=$app->Meta()->title?></title>

	<link href="/assets/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/remodal/1.1.1/remodal.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/remodal/1.1.1/remodal-default-theme.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.0/css/swiper.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css" />
    
    <link href="/assets/css/app.css?<?=md5_file( __DIR__.'/../assets/css/app.css' )?>" rel="stylesheet">
    
    <link rel="apple-touch-icon" sizes="57x57" href="/assets/images/icon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/assets/images/icon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/assets/images/icon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/assets/images/icon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/assets/images/icon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/assets/images/icon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/assets/images/icon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/assets/images/icon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/assets/images/icon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/assets/images/icon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/assets/images/icon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/assets/images/icon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/assets/images/icon/favicon-16x16.png">
    <link rel="manifest" href="/assets/images/icon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/assets/images/icon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    
    <meta property="og:type" content="website" />
    <meta property="og:url" content="https://maykop.yug-avto.ru/" />
    <meta property="og:title" content="<?=$app->Meta()->title?>" />
    <meta property="og:description" content="<?=$app->Meta()->description?>" />
    <meta property="og:image" content="https://maykop.yug-avto.ru/assets/images/og_logo.jpg"  />
	
    <?php include __DIR__.'/../include/_beforeBody.php'; ?>
	
	
    
  </head>

  <body>
	
    <?php include __DIR__.'/../include/_afterBody.php'; ?>
    
    <div class="container py-3 position-relative">
      
      <div class="row top-row">
        <div class="col-2 pt-1 pc">
          <a href="/">
            <svg xmlns="http://www.w3.org/2000/svg">
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Logo-YA"></use>
            </svg>
          </a>
        </div>
        <div class="col pc">
          <ul class="p-0 pt-3">
            <li class="pr-3"><a href="<?=(($app->Route()->section)?'/':'')?>#avail" role="scrole" data-scrole="avail">Автомобили в наличии</a></li>
            <?php // <li class="px-3"><a href="/testdrive/">Тест-драйв</a></li> ?>
            <li class="px-3"><a href="/service/">Сервис</a></li>
            <li class="px-3"><a href="/tradein/">Трейд-ин</a></li>
            <li class="px-3"><a href="/contacts/">Контакты</a></li>
            <li class="pl-3"><a href="#form_callback">Обратный звонок</a></li>
          </ul>
        </div>
        <div class="col-2 pt-1 text-right pc">
          <a href="tel:+<?=$app::phoneIn( $app->Conf()['phone'] )?>" class="phone"><i class="fa fa-phone" aria-hidden="true"></i> <span class="<?=$app->Conf()['calltouch']?>"><?=$app::phoneOut( $app->Conf()['phone'] )?></span></a>
          <p><small><?=$app->Conf()['address']?></small></p>
        </div>
        
        <div class="col-8 col-sm-8 col-lg-8 pt-1 pb-3 mob">
          <a href="/">
            <svg xmlns="http://www.w3.org/2000/svg">
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Logo-YA"></use>
            </svg>
          </a>
        </div>
        <div class="col-4 col-sm-4 col-lg-4 pt-2 pb-3 text-right mob">
          <a href="#" role="ShowMenu">
            <i class="fa fa-bars" aria-hidden="true"></i>
            <i class="fa fa-times" style="display: none;" aria-hidden="true"></i>
          </a>
        </div>
      </div>
      
      <div class="row top-row mob">
        <div class="col pt-1">
          <a href="tel:+<?=$app::phoneIn( $app->Conf()['phone'] )?>" class="phone"><i class="fa fa-phone" aria-hidden="true"></i> <?=$app::phoneOut( $app->Conf()['phone'] )?></a>
          <p><small><?=$app->Conf()['address']?></small></p>
        </div>
      </div>
      
      <div class="mob-menu">
        <ul class="p-0">
          <li class="p-2"><a href="<?=(($app->Route()->section)?'/':'')?>#avail" role="scrole" data-scrole="avail"><i class="fa fa-long-arrow-right mr-3" aria-hidden="true"></i> Автомобили в наличии</a></li>
          <?php // <li class="p-2"><a href="/testdrive/"><i class="fa fa-long-arrow-right mr-3" aria-hidden="true"></i> Тест-драйв</a></li> ?>
          <li class="p-2"><a href="/service/"><i class="fa fa-long-arrow-right mr-3" aria-hidden="true"></i> Сервис</a></li>
          <li class="p-2"><a href="/tradein/"><i class="fa fa-long-arrow-right mr-3" aria-hidden="true"></i> Трейд-ин</a></li>
          <li class="p-2"><a href="/contacts/"><i class="fa fa-long-arrow-right mr-3" aria-hidden="true"></i> Контакты</a></li>
          <li class="p-2"><a href="#form_callback"><i class="fa fa-long-arrow-right mr-3" aria-hidden="true"></i> Обратный звонок</a></li>
        </ul>
      </div>
      
    </div>
  