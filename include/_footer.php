	<div class="py-3 bg-yadarkgray footer">
      <div class="container">
        <div class="row">
          <div class="col-md-6 mb-2">
            <svg xmlns="http://www.w3.org/2000/svg">
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Logo-YA-M"></use>
            </svg>
          </div>
          <div class="col-md-6 mb-2 social">
            <?php /* foreach ( $app->Conf()['Social'] as $icon => $link ) { ?>
            <a href="<?=$link?>" target="_blank"><i class="fa fa-<?=$icon?>" aria-hidden="true"></i></a>&nbsp;
            <?php } // foreach */ ?>
          </div>
          <div class="col-md-12 my-3"></div>
          <div class="col-md-6">
            
          </div>
          <div class="col-md-6">
          </div>
        </div>
      </div>
    </div>
	
	<?php include __DIR__.'/../include/_svg.php'; ?>
	<?php include __DIR__.'/../include/_modals.php'; ?>
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/remodal/1.1.1/remodal.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.0/js/swiper.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/inputmask/4.0.6/jquery.inputmask.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    
    <script>
		var JSApp = {};
		JSApp.AvailCars = <?=json_encode($app->getAvailCars())?>;
		JSApp.AvailCars.perPage = <?=(int)$app->Conf()['page']?>;
    </script>
    
    <script src="/assets/js/app.js?<?=md5_file( __DIR__.'/../assets/js/app.js' )?>"></script>
    
    <?php include __DIR__.'/../include/_scripts.php'; ?>
  
  </body>
</html>