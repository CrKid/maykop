              <div class="alert alert-dismissible alert-success">
                <strong>Спасибо за вашу заявку!</strong> Мы свяжемся с Вами в ближайшее время.
              </div>
              <div class="alert alert-dismissible alert-danger">
                <strong>Ой, что-то пошло не так!</strong> Повторите попытку позднее.
              </div>