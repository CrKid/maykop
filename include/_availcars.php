	<div class="container" role="scrole" data-scrole="avail">
    
      <div class="row mt-5">
        <div class="col-md-12"><h1>Купить новые авто <small><small>(<?=$app->getAvailCars()['items_count']?> <?=$app->getWorld( $app->getAvailCars()['items_count'], 'a' )?>)</small></small></h1></div>
      </div>
      
      <div class="row mt-5">
        <div class="col-md-3 form-group">
          <select class="form-control" name="FilterBrand" required>
            <option selected disabled>Марка *</option>
            <?php foreach ( $app->getAvailCars()['brands'] as $brand ) { ?>
            <option value="<?=$brand['alias']?>"><?=$brand['rutitle']?></option>
            <?php } // foreach ?>
          </select>
        </div>
        <div class="col-md-3 form-group" required>
          <select class="form-control" name="FilterModel" disabled="">
            <option selected disabled>Модель *</option>
          </select>
        </div>
        <div class="col-md-5 form-group">
          <input type="hidden" name="FilterMin" value="">
          <input type="hidden" name="FilterMax" value="">
          <div id="slider-range"></div>
          <span class="pull-left mt-2" id="FilterMin"><?=number_format(intval($app->getAvailCars()['prices'][0]/10000)*10000, 0 , '', ' ');?> ₽</span>
          <span class="pull-right mt-2" id="FilterMax"><?=number_format(intval(($app->getAvailCars()['prices'][count($app->getAvailCars()['prices'])-1]+5000)/10000)*10000, 0 , '', ' ');?> ₽</span>
        </div>
        <div class="col-md-1 form-group pc">
          <a href="#" role="ClearFilter" class="but but-white text-center"><i class="fa fa-times" aria-hidden="true"></i></a>
        </div>
        <div class="col-md-1 form-group mob">
          <a href="#" role="ClearFilter" class="but but-blue d-block text-center">Очистить</a>
        </div>
        <div class="col-md-12">
          <hr />
        </div>
      </div>
      
      
      
      <div class="row my-5">
      	<?php $i = 0; ?>
          <?php foreach ( $app->getAvailCars()['models'] as $model ) { ?>
          <div class="col-sm-6 col-md-3 mb-4 <?=((intval($i/$app->Conf()['page'])+1>1)?'d-none':'')?>" role="car-card" data-page="<?=intval($i/$app->Conf()['page'])+1?>" data-car="<?=$model['brand_title']?> <?php if ( $model['title'] != 'NIVA' ) echo $model['title'];?>" data-price="<?=$model['final_price']?>" data-brand="<?=$model['brand_alias']?>" data-model="<?=$model['alias']?>">
            <div class="card border-secondary text-center">
              <div class="card-body">
                
                <img class="w-100" src="<?=$model['photo']?>" />
                <h4 class="card-title c-yadarkblue" style="min-height: 55px;"><?=$model['brand_title']?> <?php if ( $model['title'] != 'NIVA' ) echo $model['title'];?></h4>
                <p class="card-text c-yadarkgray"><span class="hc-yablack"><strong><?=$model['count']?></strong></span> в наличии</p>
                <h5><strike><?=number_format((int)$model['price'], 0, '', ' ')?></strike> ₽</h5>
                <h4 class="c-yablack">от <span class="h3 ml-2"><?=number_format((int)$model['final_price'], 0, '', ' ')?></span> ₽</h4>
                <h5 class="p-2 c-yawhite bg-yadarkblue">выгода до <?=number_format((int)$model['discount'], 0, '', ' ')?> ₽</h5>
                <hr />
                <a href="#form_order" role="Car" class="but but-yell py-2 d-block text-center" data-form="form_order" data-car="<?=$model['brand_title']?> <?php if ( $model['title'] != 'NIVA' ) echo $model['title'];?>, от <?=number_format((int)$model['final_price'], 0, '', ' ')?> ₽">Оставить заявку <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
                <?php /*
				<hr />
                <a href="#form_testdrive" role="Car" class="but but-white d-block text-center" data-form="form_testdrive" data-car="<?=$model->item->Brand->Title?> <?php if ( $model->item->Title!= 'NIVA' ) echo $model->item->Title?>"><svg xmlns="http://www.w3.org/2000/svg"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-wheel"></use></svg> Запись на тест-драйв</a>
				*/ ?>
                
              </div>
            </div>
          </div>
          <?php $i++; ?>
          <?php } // foreach models ?>
      </div>
      <?php if ( ceil(count($app->getAvailCars()['models'])/$app->Conf()['page']) > 1 ) { ?>
      <div class="row page-nav mb-5">
        <div class="col-md-12">
		  <ul class="p-0">
            <li class="p-1 mr-1"><i class="fa fa-long-arrow-left" aria-hidden="true"></i></li>
            <?php for ($p=1; $p<=ceil(count($app->getAvailCars()['models'])/$app->Conf()['page']); $p++) { ?>
            <li class="p-1 mx-1 text-center pages <?=(($p==1)?'active':'')?>" style="width: 30px;">
              <?php if ( $p!=1 ) { ?><a href="#" role="paging" data-page="<?=$p?>"><? } ?>
              <?=$p?>
              <?php if ( $p!=1 ) { ?></a><? } ?>
            </li>
            <?php } // for ?>
            <li class="p-1 ml-2"><a href="#" role="paging" data-page="2"><i class="fa fa-long-arrow-right" aria-hidden="true"></i></a></li>
            <?php if ( count($app->getAvailCars()['models']) > $app->Conf()['page'] ) { ?>
            <li class="p-1 pull-right"><a href="#" role="paging" data-page="all">Показать все</a></li>
            <?php } // if ?>
          </ul>
		</div>
      </div>
      <?php } // if ?>
    </div>