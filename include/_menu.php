<div class="bg-yadarkblue py-4 menu">
  <div class="container">
    <div class="row <?=(($app->MD->isMobile())?'':'text-center')?>">
      <div class="col-md-3 py-3">
        <a href="/actions/">
          <i class="fa fa-flag-o mr-3" aria-hidden="true"></i> 
          Акции компании
        </a>
      </div>
      <div class="col-md-3 py-3">
        <a href="/hot/">
          <i class="fa fa-fire mr-3" aria-hidden="true"></i> 
          Горячие предложения
        </a>
      </div>
      <div class="col-md-3 py-3">
        <a href="#help"  role="scrole" data-scrole="help">
          <i class="fa fa-comments-o mr-3" aria-hidden="true"></i> 
          Помочь подобрать авто
        </a>
      </div>
      <div class="col-md-3 py-3">
        <a href="#form_credit">
          <i class="fa fa-credit-card mr-3" aria-hidden="true"></i> 
          Заявка на кредит
        </a>
      </div>
    </div>
  </div>
</div>