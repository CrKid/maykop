<p>
  <a href="tel:+<?=$app::phoneIn( $app->Conf()['phone'] )?>" class="phone"><i class="fa fa-phone" aria-hidden="true"></i> <span class="<?=$app->Conf()['calltouch']?>"><?=$app::phoneOut( $app->Conf()['phone'] )?></span></a>
  <br />
  <a href="mailto:<?=$app->Conf()['email']?>"><i class="fa fa-envelope-o" aria-hidden="true"></i> <?=$app->Conf()['email']?></a>
</p>
<p><a href="yandexnavi://build_route_on_map?lat_to=<?=$app->Conf()['Coords']['lat']?>&lon_to=<?=$app->Conf()['Coords']['lon']?>"><i class="fa fa-map-marker" aria-hidden="true"></i> <?=$app->Conf()['address']?></a></p>