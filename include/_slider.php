	<div class="container-fluid">
      <div class="row banner">
      	<div class="slider-container">
          <div class="swiper-wrapper">
            <?php foreach ( $app->getBanners() as $b )  { ?>
            <div class="swiper-slide" style="background: url(<?=$b['file'].'?'.md5_file($_SERVER['DOCUMENT_ROOT'].$b['file'])?>) top center no-repeat;">
              <?php if ( $app->Conf()['Banner']['Links'][$b['link']] ) { ?>
              <a href="<?=$app->Conf()['Banner']['Links'][$b['link']]?>" class="banner-link"></a>
              <?php } ?>
            </div>
            <?php } ?>
          </div>
          <!-- Add Pagination -->
          <div class="swiper-pagination"></div>
          <!-- Add Arrows -->
          <div class="slider-next"><i class="fa fa-angle-right" aria-hidden="true"></i></div>
          <div class="slider-prev"><i class="fa fa-angle-left" aria-hidden="true"></i></div>
        </div>

      </div>
    </div>
    
    <?php if ( !$app->Route()->section ) { ?>
    <div class="py-5 bg-yalightgray" role="scrole" data-scrole="help">
      <div class="container">
        <?php include $app->getFormFile( 'form_offer' ); ?>
      </div>
    </div>
    <?php } // if ?>