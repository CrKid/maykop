<?php foreach ( $app->getForms( true ) as $f ) { ?>

<div class="remodal text-left <?=(($f=='form_service'||$f=='form_tradein')?'big':'')?>" data-remodal-id="<?=$f?>">
  <button data-remodal-action="close" class="remodal-close"></button>
  <?php include $app->getFormFile( $f, true ); ?>
</div>

<?php } // foreach ?>