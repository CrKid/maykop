'use strict';

$('input[type="phone"]').inputmask({ 'mask': '+7 (999) 999-99-99', showMaskOnHover: false });

JSApp.MainSwiper = new Swiper('.slider-container', {
	slidesPerView: 1,
	loop: true,
	effect: 'fade',
	pagination: {
		el: '.swiper-pagination',
		clickable: true,
	},
	autoplay: {
		delay: 5000,
		disableOnInteraction: true,
	},
	navigation: {
		nextEl: '.slider-next',
		prevEl: '.slider-prev',
	},
});
JSApp.FindForm = function( e ) {
	
	var f = $(e).parent();
	if ( f.is('form') ) {
		return f;
	} else {
		return JSApp.FindForm(f);
	}
}
JSApp.SetTitleForm = function( f, c ) {
	
	$('div.remodal[data-remodal-id="'+f+'"]').find('legend').text( c );
	$('div.remodal[data-remodal-id="'+f+'"]').find('input[name="Car"]').val( c );
}

JSApp.ScroleTo = function( scrole ) {
	
	$('html, body').animate({ scrollTop: $('div[data-scrole="' + scrole + '"]').offset().top }, 300);
}

JSApp.Filter = {};

JSApp.Filter.Show = function() {
	
	var
	cars = $('[role="car-card"]'+((JSApp.Filter.Data.Brand)?'[data-brand="'+JSApp.Filter.Data.Brand+'"]':'')+((JSApp.Filter.Data.Model)?'[data-model="'+JSApp.Filter.Data.Model+'"]':'')+((JSApp.Filter.Data.Page)?'[data-page="'+JSApp.Filter.Data.Page+'"]':''));
	
	$('[role="car-card"]').removeClass('d-none').addClass('d-none');
	
	if ( JSApp.Filter.Data.Min && JSApp.Filter.Data.Max ) {
	
		$(cars).each( function( i, e ) {
			
			var price = Number( $(e).data('price') );
			if ( price >= JSApp.Filter.Data.Min && price <= JSApp.Filter.Data.Max ) $(e).removeClass('d-none');
		});
			
	} else { 
		
		$(cars).removeClass('d-none');
	}
	
	$('.page-nav').removeClass('d-none').addClass('d-none');
}
JSApp.Filter.Reset = function() {
	
	$('[role="car-card"]').removeClass('d-none').addClass('d-none');
	$('[data-page="1"]').removeClass('d-none');
	
	$('.page-nav').removeClass('d-none');
	
	var html = '<option selected disabled>Модель *</option>';
	$('select[name="FilterModel"]').html( html );
	
	html = '<option selected disabled>Марка *</option>';
	for (var i in JSApp.AvailCars.brands) {
		
		html += '<option value="'+JSApp.AvailCars.brands[i].alias+'">'+JSApp.AvailCars.brands[i].title+'</option>';
	}
	$('select[name="FilterBrand"]').html( html );
	$('select[name="FilterModel"]').attr('disabled', true);
	
	$('#slider-range').slider( 'values', [ JSApp.AvailCars.prices[0], JSApp.AvailCars.prices[JSApp.AvailCars.prices.length-1] ] );
	$('span#FilterMin').text( YApps.Formatter(JSApp.AvailCars.prices[0])+' ₽' );
	$('span#FilterMax').text( YApps.Formatter(JSApp.AvailCars.prices[JSApp.AvailCars.prices.length-1])+' ₽' );
	
	JSApp.ScroleTo( 'avail' );
}
JSApp.Filter.PageNavi = function( page = 1 ) {
	
	var c = Math.ceil(JSApp.AvailCars.Count/JSApp.AvailCars.perPage);
	
	var html = '<li class="p-1 mr-1">';
	if ( page > 1 ) html += '<a href="#" role="paging" data-page="'+(page-1)+'">';
	html += '<i class="fa fa-long-arrow-left" aria-hidden="true"></i>';
	if ( page > 1 ) html += '</a>';
	html += '</li>';
	
	for ( var i = 1; i <= c; i++ ) {
		
		html += '<li class="p-1 mx-1 text-center pages '+((i==page)?'active':'')+'" style="width: 30px;">';
		if ( i != page ) html += '<a href="#" role="paging" data-page="'+i+'">';
		html += i;
		if ( i != page ) html += '</a>';
		html += '</li>';
		
	}
	
	html += '<li class="p-1 ml-1">';
	if ( page < c ) html += '<a href="#" role="paging" data-page="'+(page+1)+'">';
	html += '<i class="fa fa-long-arrow-right" aria-hidden="true"></i>';
	if ( page < c ) html += '</a>';
	
	html += '<li class="p-1 pull-right"><a href="#" role="paging" data-page="all">Показать все</a></li>';
	
	$('.page-nav ul').html( html );
	
	JSApp.ScroleTo( 'avail' );
}

$( "#slider-range" ).slider({
	
	range: true,
	min: Number(JSApp.AvailCars.prices[0]),
	
	max: JSApp.AvailCars.prices[JSApp.AvailCars.prices.length-1],
    step: 10000,
	values: [ JSApp.AvailCars.prices[0], JSApp.AvailCars.prices[JSApp.AvailCars.prices.length-1] ],
	slide: function( event, ui ) {
		
		$('span#FilterMin').text( YApps.Formatter(ui.values[0])+' ₽' );
		$('span#FilterMax').text( YApps.Formatter(ui.values[1])+' ₽' );
	},
	stop: function( event, ui ) {
		
		$('input[name="FilterMin"]').val( ui.values[0] );
		$('input[name="FilterMax"]').val( ui.values[1] );
		
		JSApp.Filter.Data = {};
		if ( $('select[name="FilterBrand"]').val() ) JSApp.Filter.Data.Brand = $('select[name="FilterBrand"]').val();
		if ( $('select[name="FilterModel"]').val() ) JSApp.Filter.Data.Model = $('select[name="FilterModel"]').val();
		JSApp.Filter.Data.Min = ui.values[0];
		JSApp.Filter.Data.Max = ui.values[1];
		JSApp.Filter.Show();
	}
});

$(document).on('change', 'select[name="FilterBrand"]', function() {
	
	var html = '<option selected disabled>Модель *</option>';
	for (var i in JSApp.AvailCars.brands[$(this).val()].models) {
		
		html += '<option value="'+JSApp.AvailCars.brands[$(this).val()].models[i].alias+'">'+JSApp.AvailCars.brands[$(this).val()].models[i].title+'</option>';
	}
	
	$('select[name="FilterModel"]').html( html );
	$('select[name="FilterModel"]').attr('disabled', false);
	
	JSApp.Filter.Data = {};
	JSApp.Filter.Data.Brand = $(this).val();
	JSApp.ScroleTo( 'avail' );
	JSApp.Filter.Show();
});

$(document).on('change', 'select[name="FilterModel"]', function() {
	
	JSApp.Filter.Data = {};
	JSApp.Filter.Data.Brand = $('select[name="FilterBrand"]').val();
	JSApp.Filter.Data.Model = $(this).val();
	JSApp.ScroleTo( 'avail' );
	JSApp.Filter.Show();
});

$(document).on('click', '[role="ClearFilter"]', function() {
	
	JSApp.Filter.Reset();
	JSApp.Filter.PageNavi(1);
	$('.mob-menu').removeClass('active');
	JSApp.ScroleTo( 'avail' );
	return false;
});

$(document).on('change', 'select[name="Brand"]', function() {
	
	var html = '<option selected disabled>Модель *</option>';
	for (var i in JSApp.AvailCars.brands[$(this).val()].models) {
		
		html += '<option value="'+JSApp.AvailCars.brands[$(this).val()].models[i].alias+'">'+JSApp.AvailCars.brands[$(this).val()].models[i].title+'</option>';
	}
	
	JSApp.FindForm( $(this) ).find('select[name="Model"]').html( html ).attr('disabled', false);
});

$(document).on('click', '[role="paging"]', function() {
	
	var page = $(this).data('page') != 'all' ? Number($(this).data('page')) : 0;
	
	$('[role="car-card"]').removeClass('d-none').addClass('d-none');
	$('[role="car-card"][data-page="'+page+'"]').removeClass('d-none');
	
	if ( page != 0 ) JSApp.Filter.PageNavi( page );
	
	if ( page == 0 ) {
		
		$('[role="car-card"]').removeClass('d-none');
		$('.page-nav').removeClass('d-none').addClass('d-none');
	}
	
	$('.mob-menu').removeClass('active');
	
	return false;
	
});

$(document).on('click', 'a[role="scrole"]', function() {
	
	JSApp.ScroleTo( $(this).data('scrole') );
	$('.mob-menu').removeClass('active');
});

$(document).on('click', 'a[role="Car"]', function() {
	
	JSApp.SetTitleForm( $(this).data('form'), $(this).data('car') );
	$('.mob-menu').removeClass('active');
});

$(document).on('click', 'a[role="ShowMenu"]', function() {
	
	$('.mob-menu').slideToggle(300);
	return false;
});
$(document).on('click', 'a[role="CloseMenu"]', function() {
	
	$('.mob-menu').removeClass('active'); return false;
});

$(document).on('click', 'a[role="SendForm"]', function() {

    JSApp.SendData = {};

    var Form = JSApp.FindForm( $(this) );
    var success = $(Form).find('.alert-success');
    var error = $(Form).find('.alert-danger');
	var Event = $(Form).data('event');

    JSApp.SendData.AppName = 'Site';
    JSApp.SendData.EventCategory = 'Заполена форма';

    JSApp.SendData.Flag = true;

    Form.find('input, select, textarea').each(function(i, e) {

        JSApp.SendData[$(e).attr('name')] = $(e).val();
        $(e).removeClass('is-invalid');

        if ($(e).attr('required')) {

            if (!$(e).val()) {

                JSApp.SendData.Flag = false;
                $(e).addClass('is-invalid');
            }
        }
    });
	
	JSApp.SendData.YandexVisitorID = YApps.Cookie.Get('_ym_uid');
	JSApp.SendData.GoogleVisitorID = YApps.Cookie.Get('_ga');
	JSApp.SendData.MatomoVisitorID = YApps.Cookie.GetMatomoID();
	
	console.log( JSApp.SendData );

    if (JSApp.SendData.Flag) {

        $.ajax({
            type: "POST",
            url: '/ajax/',
            data: JSApp.SendData,
            success: function(data) {

                var res = JSON.parse(data);

                if (res.status == 'success') {

                    $(success).slideDown(300);
                    $(error).hide();

                    if (typeof yaCounter52698295 != 'undefined') yaCounter52698295.reachGoal( JSApp.SendData.Id+'_send');
                    if (typeof Matomo != 'undefined') _paq.push(["trackEvent", 'Формы', JSApp.SendData.Form, 'Отправка']);
						
					JSApp.Layer = {
						'event': 'FormSubmission',
						'eventCategory': Event,
						'eventAction': 'submit',
						'eventLabel': JSApp.SendData.Model || null
					};
					dataLayer.push( JSApp.Layer );

                    
                    var CallTouchURL = 'https://api-node10.calltouch.ru/calls-service/RestAPI/requests/29797/register/';
                    CallTouchURL += '?subject=Формы сайта - '+JSApp.SendData.Form;
                    CallTouchURL += '&sessionId='+window.call_value_38;
                    CallTouchURL += '&fio='+YApps.SendData.Name;
                    CallTouchURL += '&phoneNumber='+YApps.SendData.Phone.replace(/[^\d;]/g, '');
					
                    $.get( CallTouchURL );
					

                } else {

                    $(error).slideDown(300);
                }
            },
            error: function() {

                $(error).slideDown(300);
            }
        });
		
		//YApps.AppPushStat( JSApp.SendData );
    }

    return false;
});





$(document).ready( function() {
	
	var anc = window.location.hash.replace("#", "");
	if ( anc ) JSApp.ScroleTo( anc );
});

