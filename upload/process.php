<?php $res = $app->importItems( $_FILES['file']['tmp_name'] );?>

<div class="container">
  <div class="row">
    <div class="col-md-12 mb-3">
      <div class="alert alert-dismissible alert-info">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        Обработано: <strong><?=count( $res['vins'] )?></strong> автомомбилей
      </div>
      <div class="alert alert-dismissible alert-success">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        Добавлено: <strong><?=$res['success']['new']?></strong> автомобилей
      </div>
      <div class="alert alert-dismissible alert-light">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        Удалено: <strong><?=$res['success']['del']?></strong> автомобилей
      </div>
      <div class="alert alert-dismissible alert-danger">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        Завершено с ошибками: <strong><?=count($res['error']['brands'])+count($res['error']['models'])?></strong>
      </div>
    </div>
    <div class="col-md-12"><h2>в базе данных: <strong><?=$app->getCountItems()?></strong> автомобилей</h2></div>
  </div>
</div>

<?php if ( $res['error'] ) { ?>

<div class="container my-5">
  <div class="row">
    <div class="col">
      <table class="table table-striped table-hover">
        <thead class="thead-dark">
          <tr>
            <th>Бренд</th>
            <th>Модель</th>
            <th>VIN</th>
            <th>Цена</th>
            <th>Максимальная скидка</th>
            <th>Минимальная цена</th>
          </tr>
        </thead>
        <tbody>
          
          <?php foreach ( $res['error']['brands'] as $b ) { ?>
          <tr>
            <td class="text-danger"><?=$b[0]?></td>
            <td ><?=$b[1]?></td>
            <td><?=$b[3]?></td>
            <td><?=$b[4]?></td>
            <td><?=$b[8]?></td>
            <td><?=$b[9]?></td>
          </tr>
          <?php } ?>
          
          <?php foreach ( $res['error']['models'] as $b ) { ?>
          <tr>
            <td><?=$b[0]?></td>
            <td class="text-danger"><?=$b[1]?></td>
            <td><?=$b[3]?></td>
            <td><?=$b[4]?></td>
            <td><?=$b[8]?></td>
            <td><?=$b[9]?></td>
          </tr>
          <?php } ?>
        
        </tbody>
      </table>
    </div>
  </div>
</div>

<?php } // if Errors ?>