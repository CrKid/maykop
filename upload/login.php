<div class="container">
  <div class="row">
    <div class="col-2"></div>
    <div class="col">
      <form method="post">
        <fieldset>
          <legend>Авторизация</legend>
          <div class="form-group">
            <input type="email" class="form-control" name="name" placeholder="Email">
          </div>
          <div class="form-group">
            <input type="password" class="form-control" name="passwd" placeholder="Password">
          </div>
          <div class="form-group">
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
        </fieldset>
      </form>
    </div>
    <div class="col-2"></div>
  </div>
</div>