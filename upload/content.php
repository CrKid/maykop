<div class="container">
  
  <div class="row my-5">
    <div class="col-md-12">
      <div class="alert alert-dismissible alert-light">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <h2>в базе данных: <strong><?=$app->getCountItems()?></strong> автомобилей</h2>
      </div>
    </div>
    <div class="col-md-8"><h1>Загрузить файл</h1></div>
    <div class="col-md-4 text-right"><a href="/upload/?action=logout"><span class="badge badge-primary">Выйти</span></a></div>
    <div class="col-md-12 my-5">
      <form method="post" enctype="multipart/form-data">
        <fieldset>
          <div class="form-group">
            <input type="file" class="form-control" name="file">
          </div>
          <div class="form-group">
            <button type="submit" class="btn btn-primary">Загрузить файл</button>
          </div>
        </fieldset>
      </form>
    </div>
  
  </div>
</div>

<?php if ($_FILES) include 'process.php'; ?>

<div class="container">
  
  <div class="row mt-5 pt-5">
    <div class="col"><h3>Пример файла <strong>.xlsx</strong>: <a href="import.xlsx">import.xlsx</a></h3></div>
  </div>
  
  <div class="row pt-5">
    <div class="col">
      <div class="alert alert-dismissible alert-light">
        <h3>Список допустимых значений брендов и моделей:</h3>
      </div>
    </div>
  </div>
  
  <div class="row">
    <div class="col">
      <div class="card-columns">
		<?php foreach ( $app->getBrands() as $b ) { ?>
          <div class="card border-dark">
            <div class="card-header"><?=$b['title']?></div>
            <div class="card-body">
              <ul>
                <?php foreach ( $app->getModelsByBrand($b['id']) as $m ) { ?>
                <li><?=$m['title']?></li>
                <?php } //foreach Models ?>
              </ul>
            </div>
          </div>
        <? } // foreach Brands ?>
      </div>
    </div>
    
  </div>
    
</div>